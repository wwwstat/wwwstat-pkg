Description: Add Tim Peeler's Debian patch for wwwstat as a quilt patch.
 - add the --optimize command-line option for low-memory systems
 - split out some processing into self-contained routines
Forwarded: yes
Author: Tim Peeler <thp@linuxforce.net>
Author: Peter Pentchev <roam@ringlet.net>
Last-Update: 2010-08-06

--- a/wwwstat.pl
+++ b/wwwstat.pl
@@ -13,6 +13,9 @@
 # See the file README  for more information.
 # See the wwwstat.1 man page for options and usage information.
 #
+# Added an option to optimize for low memory systems.
+# Tim Peeler <thp@linuxforce.net>
+
 sub usage {
     die <<"EndUsage";
 usage: $Pname [-F system_config] [-f user_config] [-helLoOuUrRvx]
@@ -24,7 +27,7 @@
                [-daily] [-hourly] [-domain] [-subdomain] [-archive] [-ident]
                [-all]   [-sort (key|byte|req)] [-top N]  [-both]
                [-no (daily|hourly|domain|subdomain|archive|ident|all)]
-               [--] [ logfile | summary | + | - ]...
+               [--optimize ] [--] [ logfile | summary | + | - ]...
 $Version
    Process a sequence of httpd Common Logfile Format access_log files and/or
    prior wwwstat output summary files (compressed if extension $Zhandle)
@@ -32,6 +35,8 @@
 Configuration options:
    -F  Get system configuration defaults from the given file.
    -f  Get user   configuration defaults from the given file.
+   --optimize     optimize for small memory systems, disables sorting
+                  for hourly, domain, subdomain, archive and ident
    --  Last option (all later arguments are treated as filenames).
 Diagnostic Options:
    -h  Help -- just display this message to STDERR and quit.
@@ -237,6 +242,9 @@
     $OldArchiveHeader   = 'Total Transfers from each Archive Section';
     $OldIdentHeader     = 'Total Transfers to each Remote Identifier';
 
+    $OptimizeMemory = 0; # should we optimize for memory usage?  will
+                         # disable sorting
+
     # The following sets the default ordering for the daily stats.
     # Change this to 1 if you always want gwstat-style output.
 
@@ -459,6 +467,10 @@
                    $TopSubdomain = $TopArchive = $TopIdent = $_;
             }
         }
+        elsif (/^optimize/)
+        {
+            $OptimizeMemory = 1;
+        }
         elsif (/^sort(.*)/)                        # Change sort method
         {
             unless ($_ = $1) {
@@ -1386,6 +1398,19 @@
 }
 
 # ==========================================================================
+# Format a percentage value
+#
+sub format_pct
+{
+    my($part, $whole) = @_;
+
+    if ($part == $whole) {
+	return '100.0';
+    }
+    return sprintf("%5.2f", 100*$part/$whole);
+}
+
+# ==========================================================================
 # Output the summary in HTML
 # 
 sub output_summary
@@ -1572,16 +1597,8 @@
     {
         $rqsts = $DayRequests{$date} || 0;
         $bytes = $DayBytes{$date} || 0;
-        if ($rqsts == $TotalRequests) {
-            $pctrqsts = "100.0";
-        } else {
-            $pctrqsts = sprintf("%5.2f", 100*$rqsts/$TotalRequests);
-        }
-        if ($bytes == $TotalBytes) {
-            $pctbytes = "100.0";
-        } else {
-            $pctbytes = sprintf("%5.2f", 100*$bytes/$TotalBytes);
-        }
+	$pctrqsts = format_pct($rqsts, $TotalRequests);
+	$pctbytes = format_pct($bytes, $TotalBytes);
         printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $date;
 
         last if ($top && (--$top == 0));
@@ -1595,7 +1612,7 @@
 sub output_hourly
 {
     local($frag) = @_;
-    local($rqsts, $bytes, $pctrqsts, $pctbytes);
+    local($rqsts, $bytes);
     local($top)    = $TopHourly;
     local($prefix) = $top ? "$PrefixTop $top" : $PrefixTotal;
 
@@ -1604,37 +1621,44 @@
     print $StartTag;
     print $StatsHeader, " Time\n";
     print $StatsRule,   "-----\n";
-    local($fmt) = "$StatsFormat  %s\n";
 
-    foreach $hour (sort hourcompare keys %HourRequests)
-    {
-        $rqsts = $HourRequests{$hour};
-        $bytes = $HourBytes{$hour};
-        if ($rqsts == $TotalRequests) {
-            $pctrqsts = "100.0";
-        } else {
-            $pctrqsts = sprintf("%5.2f", 100*$rqsts/$TotalRequests);
-        }
-        if ($bytes == $TotalBytes) {
-            $pctbytes = "100.0";
-        } else {
-            $pctbytes = sprintf("%5.2f", 100*$bytes/$TotalBytes);
+    if ($OptimizeMemory) {
+        while ( ($hour, $rqsts) = each %HourRequests) {
+            $bytes = $HourBytes{$hour};
+            &print_hourly($rqsts, $bytes);
+            last if ($top && (--$top == 0));
+        }
+    } else {
+        foreach $hour (sort hourcompare keys %HourRequests)
+        {
+            $rqsts = $HourRequests{$hour};
+            $bytes = $HourBytes{$hour};
+            &print_hourly($rqsts, $bytes);
+            last if ($top && (--$top == 0));
         }
-        printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $hour;
-
-        last if ($top && (--$top == 0));
     }
     print $EndTag;
 }
 
+sub print_hourly {
+
+    my ($rqsts, $bytes) = @_;
+    local($pctrqsts, $pctbytes);
+    local($fmt) = "$StatsFormat  %s\n";
+
+    $pctrqsts = format_pct($rqsts, $TotalRequests);
+    $pctbytes = format_pct($bytes, $TotalBytes);
+    printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $hour;
+}
+
 # ==========================================================================
 # Output the stats for each requesting client's domain/country/organization
 #
 sub output_domain
 {
     local($frag) = @_;
-    local($rqsts, $bytes, $pctrqsts, $pctbytes);
     local($top)    = $TopDomain;
+    local($rqsts, $bytes, $country);
     local($prefix) = $top ? "$PrefixTop $top" : $PrefixTotal;
 
     print "<HR>\n";
@@ -1642,37 +1666,45 @@
     print $StartTag;
     print $StatsHeader, " Domain\n";
     print $StatsRule,   "------------------------------------\n";
-    local($fmt) = "$StatsFormat %-5s %s\n";
     
-    foreach $domain (sort domaincompare keys %DomainRequests)
-    {
-        $country = $DomainMap{$domain} || '';
-        $rqsts   = $DomainRequests{$domain};
-        $bytes   = $DomainBytes{$domain};
-        if ($rqsts == $TotalRequests) {
-            $pctrqsts = "100.0";
-        } else {
-            $pctrqsts = sprintf("%5.2f", 100*$rqsts/$TotalRequests);
-        }
-        if ($bytes == $TotalBytes) {
-            $pctbytes = "100.0";
-        } else {
-            $pctbytes = sprintf("%5.2f", 100*$bytes/$TotalBytes);
+    if ($OptimizeMemory) {
+        while ( ($domain, $rqsts) = each %DomainRequests) {
+            $country = $DomainMap{$domain} || '';
+            $bytes   = $DomainBytes{$domain};
+            print_domain($country, $rqsts, $bytes);
+            last if ($top && (--$top == 0));
+        }
+    } else {
+        foreach $domain (sort domaincompare keys %DomainRequests)
+        {
+            $country = $DomainMap{$domain} || '';
+            $rqsts   = $DomainRequests{$domain};
+            $bytes   = $DomainBytes{$domain};
+            print_domain($country, $rqsts, $bytes);
+            last if ($top && (--$top == 0));
         }
-        printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $domain, $country;
-
-        last if ($top && (--$top == 0));
     }
     print $EndTag;
 }
 
+sub print_domain {
+
+    my ($country, $rqsts, $bytes) = @_;
+    local($pctrqsts, $pctbytes);
+    local($fmt) = "$StatsFormat %-5s %s\n";
+
+    $pctrqsts = format_pct($rqsts, $TotalRequests);
+    $pctbytes = format_pct($bytes, $TotalBytes);
+    printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $domain, $country;
+}
+
 # ==========================================================================
 # Output the stats for each requesting client's DNS subdomain
 #
 sub output_subdomain
 {
     local($frag) = @_;
-    local($rqsts, $bytes, $pctrqsts, $pctbytes);
+    local($rqsts, $bytes);
     local($top)    = $TopSubdomain;
     local($prefix) = $top ? "$PrefixTop $top" : $PrefixTotal;
 
@@ -1681,37 +1713,44 @@
     print $StartTag;
     print $StatsHeader, " Reversed Subdomain\n";
     print $StatsRule,   "------------------------------------\n";
-    local($fmt) = "$StatsFormat %s\n";
 
-    foreach $subdomain (sort subdomcompare keys %SubdomainRequests)
-    {
-        $rqsts = $SubdomainRequests{$subdomain};
-        $bytes = $SubdomainBytes{$subdomain};
-        if ($rqsts == $TotalRequests) {
-            $pctrqsts = "100.0";
-        } else {
-            $pctrqsts = sprintf("%5.2f", 100*$rqsts/$TotalRequests);
-        }
-        if ($bytes == $TotalBytes) {
-            $pctbytes = "100.0";
-        } else {
-            $pctbytes = sprintf("%5.2f", 100*$bytes/$TotalBytes);
+    if ($OptimizeMemory) {
+        while ( ($subdomain, $rqsts) = each %SubdomainRequests ) {
+            $bytes = $SubdomainBytes{$subdomain};
+            print_subdomain($rqsts, $bytes);
+            last if ($top && (--$top == 0));
+        }
+    } else {
+        foreach $subdomain (sort subdomcompare keys %SubdomainRequests)
+        {
+            $rqsts = $SubdomainRequests{$subdomain};
+            $bytes = $SubdomainBytes{$subdomain};
+            print_subdomain($rqsts, $bytes);
+            last if ($top && (--$top == 0));
         }
-        printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $subdomain;
-
-        last if ($top && (--$top == 0));
     }
     print $EndTag;
 }
 
+sub print_subdomain {
+
+    my ($rqsts, $bytes) = @_;
+    local($pctrqsts, $pctbytes);
+    local($fmt) = "$StatsFormat %s\n";
+
+    $pctrqsts = format_pct($rqsts, $TotalRequests);
+    $pctbytes = format_pct($bytes, $TotalBytes);
+    printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $subdomain;
+}
+
 # ==========================================================================
 # Output the stats for each archive (URL path or category)
 #
 sub output_archive
 {
     local($frag) = @_;
-    local($rqsts, $bytes, $pctrqsts, $pctbytes, $asec);
     local($top)    = $TopArchive;
+    local($rqsts, $bytes, $section);
     local($prefix) = $top ? "$PrefixTop $top" : $PrefixTotal;
 
     print "<HR>\n";
@@ -1719,37 +1758,46 @@
     print $StartTag;
     print $StatsHeader, " Archive Section\n";
     print $StatsRule,   "------------------------------------\n";
-    local($fmt) = "$StatsFormat %s\n";
 
-    foreach $section (sort archivecompare keys %ArchiveRequests)
-    {
-        $rqsts = $ArchiveRequests{$section};
-        $bytes = $ArchiveBytes{$section};
-        next unless $rqsts;
-        if ($rqsts == $TotalRequests) {
-            $pctrqsts = "100.0";
-        } else {
-            $pctrqsts = sprintf("%5.2f", 100*$rqsts/$TotalRequests);
-        }
-        if ($bytes == $TotalBytes) {
-            $pctbytes = "100.0";
-        } else {
-            $pctbytes = sprintf("%5.2f", 100*$bytes/$TotalBytes);
-        }
-        $asec = $section;
-        $asec =~ s/\&/\&amp;/g;      # Replace HTML specials
-        $asec =~ s/</\&lt;/g;
-        $asec =~ s/>/\&gt;/g;
-        if ($InsertLink && ($asec =~ m:^/:)) {
-            $asec = "<a href=\"$asec\">$asec</a>";
+    if ($OptimizeMemory) {
+        while ( ($section, $rqsts) = each %ArchiveRequests) {
+            $bytes = $ArchiveBytes{$section};
+            next unless $rqsts;
+            print_archive($rqsts, $bytes, $section);
+            last if ($top && (--$top == 0));
+        }
+    } else {
+        foreach $section (sort archivecompare keys %ArchiveRequests)
+        {
+            $rqsts = $ArchiveRequests{$section};
+            $bytes = $ArchiveBytes{$section};
+            next unless $rqsts;
+            print_archive($rqsts, $bytes, $section);
+            last if ($top && (--$top == 0));
         }
-        printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $asec;
-
-        last if ($top && (--$top == 0));
     }
     print $EndTag;
 }
 
+sub print_archive {
+
+    my ($rqsts, $bytes, $section) = @_;
+
+    local($pctrqsts, $pctbytes, $asec);
+    local($fmt) = "$StatsFormat %s\n";
+
+    $pctrqsts = format_pct($rqsts, $TotalRequests);
+    $pctbytes = format_pct($bytes, $TotalBytes);
+    $asec = $section;
+    $asec =~ s/\&/\&amp;/g;      # Replace HTML specials
+    $asec =~ s/</\&lt;/g;
+    $asec =~ s/>/\&gt;/g;
+    if ($InsertLink && ($asec =~ m:^/:)) {
+        $asec = "<a href=\"$asec\">$asec</a>";
+    }
+    printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $asec;
+}
+
 # ==========================================================================
 # Output the stats for each calendar day represented in the input file(s)
 #
@@ -1765,29 +1813,36 @@
     print $StartTag;
     print $StatsHeader, " Remote Identity\n";
     print $StatsRule,   "------------------------------------\n";
-    local($fmt) = "$StatsFormat %s\n";
 
-    foreach $ident (sort identcompare keys %IdentRequests)
-    {
-        $rqsts = $IdentRequests{$ident};
-        $bytes = $IdentBytes{$ident};
-        if ($rqsts == $TotalRequests) {
-            $pctrqsts = "100.0";
-        } else {
-            $pctrqsts = sprintf("%5.2f", 100*$rqsts/$TotalRequests);
-        }
-        if ($bytes == $TotalBytes) {
-            $pctbytes = "100.0";
-        } else {
-            $pctbytes = sprintf("%5.2f", 100*$bytes/$TotalBytes);
+    if ($OptimizeMemory) {
+        while ( ($ident, $rqsts) = each %IdentRequests) {
+            $bytes = $IdentBytes{$ident};
+            print_ident($rqsts, $bytes);
+            last if ($top && (--$top == 0));
+        }
+    } else {
+        foreach $ident (sort identcompare keys %IdentRequests)
+        {
+            $rqsts = $IdentRequests{$ident};
+            $bytes = $IdentBytes{$ident};
+            print_ident($rqsts, $bytes);
+            last if ($top && (--$top == 0));
         }
-        printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $ident;
-
-        last if ($top && (--$top == 0));
     }
     print $EndTag;
 }
 
+sub print_ident {
+
+    my ($rqsts, $bytes) = @_;
+    local($pctrqsts, $pctbytes);
+    local($fmt) = "$StatsFormat %s\n";
+
+    $pctrqsts = format_pct($rqsts, $TotalRequests);
+    $pctbytes = format_pct($bytes, $TotalBytes);
+    printf $fmt, $pctrqsts, $pctbytes, $bytes, $rqsts, $ident;
+}
+
 # ==========================================================================
 # ==========================================================================
 # The following sort comparison functions take $a and $b as the two
