Copyright (c) 1994, 1996 Regents of the University of California.
See the file LICENSE for Licensing and Redistribution Information.
See the file README  for more information.
==========================================================================
The latest version of wwwstat can be obtained from

       http://www.ics.uci.edu/pub/websoft/wwwstat/
        ftp://www.ics.uci.edu/pub/websoft/wwwstat/

==========================================================================
Installation instructions:

0. You need Perl (4.036 or 5.002, or better) to run wwwstat.
   If you don't have it already, see <http://www.perl.com/perl/>
   for information on how to download and install perl.

1. Get the wwwstat package from the distribution site (above).  Normally,
   it will be in the form of a gzip'd unix tar file.  If it has not
   already been decompressed by your WWW client, than do:

        % gunzip wwwstat-2.0.tar.gz

   depending on which version (the 2.0) you downloaded.  gunzip is part
   of the gzip compression/decompression package, which can be obtained
   from any Gnu software archive.

2. Move the resulting wwwstat-2.0.tar file to the directory above
   where you want to install wwwstat, cd to that directory, and do

        % tar xvf wwwstat-2.0.tar

   to create the directory ./wwwstat-2.0 containing the following:

        Changes       -- the list of known problems and version information
        FAQ           -- frequently asked questions
        INSTALL       -- this file
        LICENSE       -- Licensing and Redistribution information
        Makefile      -- the makefile (for creating executables)
        README        -- general intro and pointers to more information
        domains.pl    -- a mapping of Internet domains to country/org names
        example.html  -- an example of what wwwstat output should look like
        monthly.pl    -- an example perl script for monthly log rotation
        oldlog2new.pl -- A tool for converting old NCSA logs to CLF format
        splitlog.1    -- the splitlog manual page
        splitlog.html -- the splitlog.1 manual converted to HTML format
        splitlog.ps   -- the splitlog.1 manual converted to postscript
        splitlog.pl   -- the main perl script for splitting the access_log
        splitlog.rc   -- an example user configuration file for splitlog
        wwwerrs.pl    -- an example perl script for reading the error_log
        wwwstat.1     -- the wwwstat manual page
        wwwstat.html  -- the wwwstat.1 manual converted to HTML format
        wwwstat.ps    -- the wwwstat.1 manual converted to postscript
        wwwstat.pl    -- the main perl script for analyzing the access_log
        wwwstat.rc    -- an example user/system configuration file

3. Edit the Makefile according to the instructions contained therein.
   Most likely, you only need to change the value of PERLBIN. 

4. Run "make" in that directory.  If your system does not have make, then
   you will have to manually edit the first line of each script, as in

        cp wwwstat.pl wwwstat
        chmod 755 wwwstat
        vi wwwstat

   and substitute

        #!YOUR_PERL_BINARY
   with
        #!/usr/local/bin/perl

   or whatever is the pathname for your perl executable.

5. Edit the wwwstat.rc file so that it corresponds to the default
   configuration for your server.  The example file includes a complete
   description of each configurable parameter.

   If needed, you can also directly edit the wwwstat script to
   change any of the default settings.

   You can do the same for splitlog via splitlog.rc.

6. Read the man pages

        % nroff -man wwwstat.1  | more
        % nroff -man splitlog.1 | more

   which includes complete information on all the wwwstat and splitlog
   options and usage.  The manuals are also available in HTML and postscript.

7. That's it.  You should now be able to run wwwstat, e.g.

        % wwwstat > results.html

8. Optionally, you may want to install the script and its library files
   in generally accessible locations, as in

        % cp -p wwwstat /usr/local/bin/
        % cp -p wwwstat.rc /usr/local/lib/perl/
        % cp -p domains.pl /usr/local/lib/perl/
        % cp -p wwwstat.1 /usr/local/man/man1/

   I recommend that splitlog only be installed by a webmaster, since it
   won't be very useful for people with non-shared logfiles.

9. If wwwstat's libraries are not installed in the normal perl library,
   then you should add their location to the path in the environment
   variable PERLLIB, as in

        % setenv PERLLIB $PERLLIB:/usr/wherever/wwwstat-2.0

If you have some old (prior to NCSA httpd_1.2) logfiles that you want
converted to the common logfile format, you will also need to customize
the oldlog2new script. Usage information can be obtained via the -h option.

The monthly.pl and wwwerrs.pl scripts are provided as examples, which
may be very useful to some webmasters.  However, they also require an
understanding of perl (i.e., they are completely undocumented aside
from the program comments).  They are not part of the wwwstat program.
