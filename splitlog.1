.\" Copyright (c) 1996 Regents of the University of California.
.\" All rights reserved.  The file LICENSE (the Artistic License)
.\" specifies the terms and conditions for redistribution.
.\"
.if n .tr \--
.if n .tr \(buo
.if n .tr \(em-
.TH splitlog 1 "03 November 1996"
.if n .ad l
.SH NAME
splitlog \- split WWW server (httpd) access logfiles
.SH SYNOPSIS
.HP 8
.B splitlog
.RB [ \-f
.IR configfile ]
.RI [ options ...]
.RB [ -- ]
.if n .br
.RI "[ " "logfile" " | "
.BR + " | " \- " ]..."
.SH DESCRIPTION
.nh
.B splitlog
reads a sequence of
.B httpd
common logfile format (CLF) access_log files and/or the standard input
and splits the logfile entries into separate files according to the
entry's requested URL or virtual host prefix.
.LP
.B splitlog
is intended to be run periodically by the webmaster as a means for providing
individual logfiles for each of the customers of a server, since it is less
efficient for the server itself to generate multiple logfiles.
.B splitlog
does not make any changes to the input file and can be configured to write the
split files in any directory.  
By default, a cached DNS lookup is performed on any IP addresses which are
unresolved in the input file.  The log entries can also be anonymized
if there are concerns about the requesting clients' privacy.
.LP
.B splitlog
is a
.B perl
script, which means you need to have a
.B perl
interpreter to run the program.  It has been tested with 
.B perl
versions 4.036 and 5.002.
.SH OPTIONS
.SS Configuration Options
These options define how
.B splitlog
should establish defaults and interpret the command-line.
.TP
.BI \-f " filename"
Get the configuration defaults from the given file.  If used, this
.B must
be the first argument on the command-line, since it needs to be interpreted
before the other command options.  The file
.B splitlog.rc
is included with the distribution as an example of this file; it contains
.B perl
source code which directly sets the control and display options provided by
.B splitlog
and contains a function for altering the split logfile name-selection
algorithm.  If
.I filename
is not a pathname, the include path (see
.BR FILES )
is searched for
.IR filename .
An empty string as
.I filename
will disable this feature.
.RB [ \-f
"splitlog.rc"]
.TP
.B --
Last option (the remaining arguments are treated as input files).
.SS Diagnostic Options
These options provide information about
.B splitlog
usage or about some unusual aspects of the logfile(s) being processed.
.TP
.B \-h
Help \(em display usage information to STDERR and then exit.
.TP
.B \-e
Display to STDERR all invalid log entries. Invalid log entries can occur
if the server is miswriting or overwriting its own log, if the request is
made by a broken client or proxy, or if a malicious attacker is trying to
gain privileged access to your system.
.SS Process Options
These options modify how and where logfile entries are written.
.TP
.B \-x
Discard any logfile entries without a filename key instead of placing
them in a special OTHERS.log.
.TP
.B \-v
Use a prefix of the input file entries (ended by the first ":" or space)
for selecting the output filename instead of, or in addition to, the URL path.
The most likely use for such a prefix is for the requested virtual host.
.TP
.BI \-dir " directory"
Place the output logfiles in the given directory instead of the
current working directory.
.TP
.BI \-anon " imu"
Anonymize the logfile entries before writing them to split logs.
The value is some combination of the letters "i" (ident field is removed),
"m" (machine name is replaced with ANON or 0), and
"u" (authentication userid field is removed).
.LP
.PD 0
.B \-dns
.TP
.B \-nodns
.PD
Do
.RB ( \-dns )
or don't
.RB ( \-nodns )
use the system's hostname lookup facilities to find the DNS hostname
associated with any unresolved IP addresses. Looking up a DNS name may be
.B very
slow, particularly when the results are negative (no DNS name),
which is why a caching capability is included as well.
.RB [ \-dns ]
.TP
.BI \-cache " filename"
Use the given DBM database as the read/write persistent DNS cache
(the .dir and .pag extensions are appended automatically). Cached entries
(including negative results) are removed after the time configured for
$DNSexpires [two months].  No caching is performed if
.I filename
is the empty string, which may be needed if your system does not support
DBM or NDBM functionality. Running
.B \-dns
without a persistent cache is not recommended.
.RB [ "\-cache"
"dnscache"]
.SS Search Options
These options are used to include or exclude logfile entries from being output
according to whether or not they match a given pattern.
The pattern is supplied in the form of a
.BR "perl regular expression" ,
except that the characters "+" and "." are escaped automatically
unless the
.B \-noescape
option is given.
Enclose the pattern in single-quotes to prevent the command shell
from interpreting some special characters.
Multiple occurrences of the same option results in an OR-ing of the
regular expressions.
.LP
.PD 0
.BI \-a " regexp"
.TP
.BI \-A " regexp"
.PD
Include
.RB ( \-a )
or exclude
.RB ( \-A )
all requests containing a hostname/IP address
matching the given perl regular expression.
.LP
.PD 0
.BI \-c " regexp"
.TP
.BI \-C " regexp"
.PD
Include
.RB ( \-c )
or exclude
.RB ( \-C )
all requests resulting in an
.B HTTP
status code
matching the given perl regular expression.
.LP
.PD 0
.BI \-d " regexp"
.TP
.BI \-D " regexp"
.PD
Include
.RB ( \-d )
or exclude
.RB ( \-D )
all requests occurring on a date (e.g., "Feb 02 1994")
matching the given perl regular expression.
.LP
.PD 0
.BI \-t " regexp"
.TP
.BI \-T " regexp"
.PD
Include
.RB ( \-t )
or exclude
.RB ( \-T )
all requests occurring during the hour (e.g., "23" is 11pm \- 12pm)
matching the given perl regular expression.
.LP
.PD 0
.BI \-m " regexp"
.TP
.BI \-M " regexp"
.PD
Include
.RB ( \-m )
or exclude
.RB ( \-M )
all requests using an HTTP method (e.g., "HEAD")
matching the given perl regular expression.
.LP
.PD 0
.BI \-n " regexp"
.TP
.BI \-N " regexp"
.PD
Include
.RB ( \-n )
or exclude
.RB ( \-N )
all requests on a URL (archive name)
matching the given perl regular expression.
.TP
.B \-noescape
Do not escape the special characters ("+" and ".") in the remaining
search options.
.SH INPUT
After parsing the options, the remaining arguments on the command-line
are treated as input arguments and are read in the order given.
If no input arguments are given, the configured default logfile is read
.RB [ + ].
.TP
.B \-
Read from standard input (STDIN).
.TP
.B +
Read the default logfile. [as configured]
.TP
.IR logfile ...
Read the given logfile.  If the
.IR logfile 's
extension indicates that is is compressed (gz|z|Z), then pipe it through
the configured decompression program
.RB [ "gunzip \-c" ]
first.
.SH USAGE
In most cases,
.B splitlog
is run on a periodic basis by a wrapper program as a
.B crontab
entry shortly after midnight, typically in conjunction
with rotating the current logfile.  The
.B \-D today
option can be used to split the main logfile on a daily basis without
rotation.
.LP
All of the command-line options, and a few options that are not available
from the command-line, can be changed within the user configuration file (see
.BR splitlog.rc ).
This file is actually a
.B perl
library module which is executed as part of the program's initialization.
The example provided with the distribution includes complete documentation
on what variables can be set and their range of values.
If the default algorithm for selecting the split logfile
name isn't desired, or if some set of names should be combined into a
single file, then uncomment the user_path_map() function and define
your own name-selection algorithm.
.LP
The
.B wwwstat
program can be used to analyze the resulting logfiles. See
.B wwwstat
for a description of the common logfile format.
.SS Perl Regular Expressions
The Search Options and many of the configuration file settings
allow for full use of perl regular expressions
(with the exception that the \-a, \-A, \-n and \-N options treat '+' and '.'
characters as normal alphabetic characters unless they are preceded by the
.B \-noescape
option).  Most people only need to know the following special characters:
.LP
.PD 0
.TP 8
.B ^
at start of pattern, means "starts with pattern".
.TP
.B $
at end of pattern, means "ends with pattern".
.TP
.B (...)
groups pattern elements as a single element.
.TP
.B ?
matches preceding element zero or one times.
.TP
.B *
matches preceding element zero or more times.
.TP
.B +
matches preceding element one or more times.
.TP
.B .
matches any single character.
.TP
.B [...]
denotes a class of characters to match. [^...] negates the class.
Inside a class, '-' indicates a range of characters.
.TP
.B (A|B|C)
matches if A or B or C matches.
.PD
.LP
Depending on your command shell, some special characters may need to be
escaped on the command line or enclosed in single-quotes to avoid shell
interpretation.
.SH ENVIRONMENT
.TP 12
.B HOME
Location of user's home directory, placed on INC path.
.TP
.B LOGDIR
Used instead of HOME if latter is undefined.
.TP
.B PERLLIB
A colon-separated list of directories in which to look for
the user configuration file.
.SH FILES
Unless a pathname is supplied, the configuration file is obtained from
the current directory, the user's home directory
.RB ( HOME " or " LOGDIR ),
the standard library path
.RB ( PERLLIB ),
and the directory indicated by the command pathname (in that order).
.TP 15
.B splitlog.rc
User configuration file.
.LP
.PD 0
.B dnscache.dir
.TP 15
.B dnscache.pag
.PD
DBM files for persistent DNS cache.
.SH SEE ALSO
.BR crontab (1),
.BR httpd (1m),
.BR perl (1),
.BR wwwstat (1)
.LP
More info and the latest version of splitlog can be obtained from
.LP
     http://www.ics.uci.edu/pub/websoft/wwwstat/
      ftp://www.ics.uci.edu/pub/websoft/wwwstat/
.LP
If you have any suggestions, bug reports, fixes, or enhancements,
please join the <wwwstat-users@ics.uci.edu> mailing list by sending
e-mail with "subscribe" in the subject of the message to the request
address <wwwstat-users-request@ics.uci.edu>.  The list is archived at
the above address.
.SS More About Perl
.TP
The Perl Language Home Page
http://www.perl.com/perl/index.html
.TP
Johan Vromans' Perl Reference Guide
http://www.xs4all.nl/~jvromans/perlref.html
.SH AUTHOR
Roy Fielding (fielding@ics.uci.edu), University of California, Irvine.
Please do not send questions or requests to the author, since the number
of requests has long since overwhelmed his ability to reply, and all
future support will be through the mailing list (see above).
.LP
This work has been sponsored in part by the Defense Advanced Research Projects
Agency under Grant Numbers MDA972-91-J-1010 and F30602-94-C-0218.
This software does not necessarily reflect the position or policy of the
U.S. Government and no official endorsement should be inferred.
