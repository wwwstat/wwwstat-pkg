wwwstat: httpd logfile analysis package

Copyright (c) 1994, 1996 Regents of the University of California.

    wwwstat processes a sequence of httpd Common Logfile Format access_log
    files and prior summary outputs, and then outputs a summary of
    the access statistics in a nice HTML format.

    splitlog processes a sequence of httpd Common Logfile Format access_log
    files (or CLF with a one-field prefix) and splits the entries into
    separate files according to the requested URL and/or vhost prefix.

==========================================================================
The latest version of wwwstat can be obtained from

       http://www.ics.uci.edu/pub/websoft/wwwstat/
        ftp://www.ics.uci.edu/pub/websoft/wwwstat/

==========================================================================
See the file LICENSE for Licensing and Redistribution Information.
See the file INSTALL for Installation Instructions.
See the file FAQ     for Frequently Asked Questions.
See the file Changes for Version History and Known Problems.

Please read the man pages, e.g.,

     % nroff -man wwwstat.1 | more
     % nroff -man splitlog.1 | more

which include complete information on all the wwwstat and splitlog options
and usage.  The man pages are also available in postscript and HTML.

This software has been developed by Roy Fielding <fielding@ics.uci.edu>
as part of the WebSoft project at the University of California, Irvine.
See the man pages or Changes for information on how to join the
wwwstat-users mailing list.

This work has been sponsored in part by the Defense Advanced Research Projects
Agency under Grant Numbers MDA972-91-J-1010 and F30602-94-C-0218.
This software does not necessarily reflect the position or policy of the
U.S. Government and no official endorsement should be inferred.
