# ==========================================================================
# This is a configuration file for wwwstat.
#
#         <http://www.ics.uci.edu/pub/websoft/wwwstat/>
#
# Please note that this is a perl include file, and thus may contain
# anything that might be found in a perl script.  However, it SHOULD ONLY
# contain variable assignments which override the default values in wwwstat's
# &init_defaults procedure (all global variables).  Most options can also
# be overridden on the command-line.
#
# wwwstat allows for two optional config files:
#        wwwstat.rc   (normally where wwwstat is installed)
#        .wwwstatrc   (normally in the user's home directory)
# which are read in that order.  The file pathnames can be changed using the
# -F and -f options, respectively.  If no path is supplied, then both files
# are looked for on the include path, which is
#
#     .:$ENV{'HOME'}:$ENV{'PERLLIB'}:<path_to_wwwstat>
#
# All wwwstat defaults are listed below, but commented-out.  You can change
# the associated value by removing the ## comment and changing the assignment.
# Use the command "perl -wc wwwstat.rc" to test the syntax (ignore typos).
#
# ==========================================================================
# Mapping of Request URI to Archive Entry [default is none]
#
# ArchiveMap is an ordered list of string match and replacement patterns
# in the form of an Nx3 array.  Matches against the request-URI are
# attempted in the listed order and the first match wins, with the result
# being that the archive pathkey is changed to the replacement string.
# If no patterns match, the request-URI remains the pathkey.
#     The first value is 'i' (case-insensitive) or 's' (case-sensitive).
#     The second value is the regexp to match against the request-URI
#     The third value is the replacement string if the pattern matches.
# If the replacement is '' or undef, then the request entry is excluded
# from all summary counts (this loses information).
# No replacements are made if the -n, -c, or -C, options are in use.
#
# The array value can be set en masse
#
##  @ArchiveMap = (
##      's', '^/Icons/\w', 'All Icons (site)',
##      's', '^/icons/\w', 'All Icons (server)',
##      'i', '/hidden/',   'All Hidden',
##  );
#
# or to append, use: push(@ArchiveMap,'i','pattern','replacement'); e.g.,
#
##  push(@ArchiveMap,'i','\.gif$','All GIFs');
#
# To insert, use: unshift(@ArchiveMap,'i','pattern','replacement'); 
# Other array modifiers (see perl manual): splice, pop, shift.
#
# ==========================================================================
# Mapping of Internet Domain to Country/Organization Name
#
# The file domains.pl initializes the domain --> Country/Org mapping as
# an associative array.  wwwstat matches domains to names using a
# suffix match of domain components, with the first (longest) match being
# the categorization for the stats-by-domain listing.  These values can be
# added to or overridden here, e.g.
#
##  $DomainMap{'uci.edu'}     = 'University of California, Irvine';
##  $DomainMap{'ics.uci.edu'} = 'UCI Information and Computer Science';
##  $DomainMap{'co.uk'}       = 'UK Commercial';
#
# NOTE: The key must be in lowercase.
#
# ==========================================================================
# Output Options  [defaults are what is listed in assignment]
#
# Specify the URL of the previous summary period (for use only as a
# hypertext link).  Set it = "" if link is unwanted.
# The three-letter abbrev for last month is substituted at any "%M".
# The four-number year of last month is substituted at any "%Y".
# Note that this is the month prior to the earliest one in this summary.
#
##  $LastSummary = "stats-%Y/%M.wwwstats.html.gz";
#
# Specify the perl regex pattern for directory index files. This will be
# dependent on the server's DirectoryIndex config (usually in srm.conf).
# 'index.html?'
#     matches "index.html" and "index.htm"
# 'index(.(html?|cgi))?'
#     matches "index", "index.html", "index.htm", and "index.cgi"
# '(([Oo]verview)|(index)).html?' 
#     matches "Overview.html", "Overview.htm", "index.html", "index.htm",
#             "overview.html", and "overview.htm"
#
##  $DirectoryIndex = 'index(.(html?|cgi))?';
#
# Specify the default location of your access log
#
##  $DefaultLog = '/usr/local/etc/httpd/logs/access_log';
#
# Specify the command for displaying compressed files to STDOUT
#
##  $Zcat    = 'gunzip -c';   # specify as null string if none are available
##  $Zhandle = '(gz|Z|z)';    # list file extensions that indicate compressed
#
# If address in log entry is one word (a local host), append what?
# Specify an appropriate '.sub.dom.ain'
#
##  $AppendToLocalhost = '.nowhere.com';
#
# Specify whether (1) or not (0) you want to display the IP address
# on reversed subdomain listings as follows:
#
##  $LocalFullAddress  = 0;    # Show full address for local hosts?
##  $OthersFullAddress = 0;    # Show full address for non-local hosts?
##  $ShowUnresolved    = 0;    # Show all unresolved addresses?
#
# Specify whether (1) or not (0) you want to insert a hypertext
# anchor for each URL in the archive section (-link option).
#
##  $InsertLink        = 0;
#
# Specify whether (1) or not (0) you want to add CGI output headers
# before the HTML summary.
# 
##  $Do_CGI            = 0;
# 
# Specify whether (1) or not (0) you want the Archive URL to be
# truncated (this saves output space and runtime memory, but loses info).
#
##  $TruncateLevel     = 0;    # Truncate after Nth hierarchy level?
##  $TruncateFile      = 0;    # Truncate any trailing filename?
#
# Specify whether (1) or not (0) you want to lookup unresolved
# IP addresses via DNS. Note that this could be *very* slow, particularly
# for those machines that do not have DNS names (e.g., SLIP/PPP clients).
#
##  $LookupDNS         = 0;
##  $DNScachefile      = 'dnscache';   # DBM file for persistent caching
##  $DNSexpires        = 5356800;      # Cache for two months (in seconds)
#
# The following lines define the visible header fields for HTML output.
# They may be changed/nationalized, but changing some may affect the
# parsing algorithm for reading old summary files.  Test all changes!
#
##  $OutputTitle        = 'World Wide Web Access Statistics for www' .
##                        $AppendToLocalhost;
#
##  $UpdateHeader       = 'Last updated: ';
##  $LastSumHeader      = 'Previous Full Summary Period';
##  $TotalsHeader       = 'Totals for Summary Period:  ';
##  $ReqRcvHeader       = 'Requests Received During Summary Period ';
##  $BtransHeader       = 'Bytes Transmitted During Summary Period ';
##  $AvgReqHeader       = 'Average Requests Received Daily         ';
##  $AvgByteHeader      = 'Average Bytes Transmitted Daily         ';
#
##  $TotalsFormat       = "%s %14.0f\n";
# 
##  $StatsHeader        = '%Reqs %Byte  Bytes Sent  Requests  ';
##  $StatsRule          = '----- ----- ------------ -------- |';
##  $StatsFormat        = '%s %s %12.0f %8d |';
# 
##  $PrefixTop          = 'Top';
##  $PrefixTotal        = 'Total';
#
##  $DailyHeader        = 'Transfers by Request Date';
##  $HourlyHeader       = 'Transfers by Request Hour';
##  $DomainHeader       = 'Transfers by Client Domain';
##  $SubdomainHeader    = 'Transfers by Reversed Subdomain';
##  $ArchiveHeader      = 'Transfers by URL/Archive Section';
##  $IdentHeader        = 'Transfers by Remote Identity';
#
# These Old headers are for reading old summary files
#
##  $OldDailyHeader     = 'Daily Transmission Statistics';
##  $OldHourlyHeader    = 'Hourly Transmission Statistics';
##  $OldDomainHeader    = 'Total Transfers by Client Domain';
##  $OldSubdomainHeader = 'Total Transfers by Reversed Subdomain';
##  $OldArchiveHeader   = 'Total Transfers from each Archive Section';
##  $OldIdentHeader     = 'Total Transfers to each Remote Identifier';
#
# The following sets the default ordering for the daily stats.
# Change this to 1 if you always want gwstat-style output.
#
##  $ReverseDateSort = 0;    # Display daily stats in reverse order.
#
# The following sets the default sort key for each section,
#    where 0 means sort by the primary key value (day, hour, domain, ...)
#          1 means sort by the number of requests
#          2 means sort by the number of bytes transferred
#
##  $SortDaily       = 0;
##  $SortHourly      = 0;
##  $SortDomain      = 0;
##  $SortSubdomain   = 0;
##  $SortArchive     = 0;
##  $SortIdent       = 0;
#
# If the output of a section is sorted, you may also want to restrict
# the output to only the N best in that section.
#
##  $TopDaily        = 0;
##  $TopHourly       = 0;
##  $TopDomain       = 0;
##  $TopSubdomain    = 0;
##  $TopArchive      = 0;
##  $TopIdent        = 0;
#
# On the other hand, you may want to exclude (0) an entire section.
# If set = 2, the top N is done first and then followed by normal section.
#
##  $Do_Daily        = 1;    # Display the Daily Statistics
##  $Do_Hourly       = 1;    # Display the Hourly Statistics
##  $Do_Domain       = 1;    # Display the Domain (Country) Statistics
##  $Do_Subdomain    = 1;    # Display the Subdomain Statistics
##  $Do_Archive      = 1;    # Display the Archive Statistics
##  $Do_Ident        = 0;    # Display the Identity Statistics (DON'T PUBLISH)
#
# The rest of these options are normally only changed on the command-line
#
##  $Verbose         = 0;    # Display   valid log entries on STDERR?
##  $PrintInvalids   = 0;    # Display invalid log entries on STDERR?
##  $PrintNonexist   = 0;    # Display requests resulting in HTTP errors?
#
##  $SearchAddress   = '';   # Pattern to look for in hostname/IP addresses.
##  $SearchCode      = '';   # Pattern to look for in Code.
##  $SearchDate      = '';   # Pattern to look for in Date.
##  $SearchTime      = '';   # Pattern to look for in Hour.
##  $SearchArchive   = '';   # Pattern to look for in Archive names.
##  $SearchMethod    = '';   # Pattern to look for in Method.
#
##  $NotAddress      = '';   # Pattern to reject entry if in IP addresses.
##  $NotCode         = '';   # Pattern to reject entry if in Code.
##  $NotDate         = '';   # Pattern to reject entry if in Date.
##  $NotTime         = '';   # Pattern to reject entry if in Hour.
##  $NotArchive      = '';   # Pattern to reject entry if in Archive names.
##  $NotMethod       = '';   # Pattern to reject entry if in Method.
#
##  $EscapeSpecials  = '[+.]';   # Most users forget about regexp syntax
#                                # so we escape these in -a -A -n -N
# ==========================================================================
1; # This must be the last line
