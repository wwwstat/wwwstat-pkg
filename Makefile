# This Makefile is used to configure the perl scripts so that
# they all use the correct pathname for the perl interpreter.
# It also makes the programs executable and creates links from "GET"
# to the other commonly-used methods.  Use the following command:
#
#     % make all
#
# You need to change the following line to the full pathname
# of your perl interpreter

PERLBIN = /usr/local/bin/perl

# and here are a some commands that may be system-dependent

COPY    = cp -p

# The rest should be automatic

CLIENTS = wwwstat wwwerrs monthly splitlog oldlog2new
LIBS    = domains.pl wwwstat.rc splitlog.rc

.SUFFIXES:
.SUFFIXES: .pl $(SUFFIXES)

all: $(CLIENTS)

.pl:
	cp $*.pl $*
	$(PERLBIN) -pi -e 's#YOUR_PERL_BINARY#$(PERLBIN)#o;' $*
	chmod 755 $*

#
# A "make clean" removes what "make all" produced
#

clean:
	$(RM) $(CLIENTS)

