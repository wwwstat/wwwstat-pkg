# Mapping of Internet Domain to Country/Organization Name
#
# The country codes are based on ISO 3166 two-character codes, but what
# is important to us is that they correspond to the names assigned by
# IANA for Internet domains.  NOTE that these change over time!
#
# wwwstat matches domains to names using a suffix match of domain
# components, with the first (longest) match being the categorization
# for the stats-by-domain listing.  These values can be added to or
# overridden in the wwwstat.rc file, e.g.
#
#     $DomainMap{'uci.edu'} = 'University of California, Irvine';
#
# NOTE: The first (key) column below must be in lowercase.

%DomainMap = (
   'ad',   'Andorra',
   'ae',   'United Arab Emirates',
   'af',   'Afghanistan',
   'ag',   'Antigua and Barbuda',
   'ai',   'Anguilla',
   'al',   'Albania',
   'am',   'Armenia',
   'an',   'Netherlands Antilles',
   'ao',   'Angola',
   'aq',   'Antarctica',
   'ar',   'Argentina',
   'as',   'American Samoa',
   'at',   'Austria',
   'au',   'Australia',
   'aw',   'Aruba',
   'az',   'Azerbaijan',
   'ba',   'Bosnia and Herzegovina',
   'bb',   'Barbados',
   'bd',   'Bangladesh',
   'be',   'Belgium',
   'bf',   'Burkina Faso',
   'bg',   'Bulgaria',
   'bh',   'Bahrain',
   'bi',   'Burundi',
   'bj',   'Benin',
   'bm',   'Bermuda',
   'bn',   'Brunei Darussalam',
   'bo',   'Bolivia',
   'br',   'Brazil',
   'bs',   'Bahamas',
   'bt',   'Bhutan',
   'bv',   'Bouvet Island',
   'bw',   'Botswana',
   'by',   'Belarus',
   'bz',   'Belize',
   'ca',   'Canada',
   'cc',   'Cocos (Keeling) Islands',
   'cf',   'Central African Republic',
   'cg',   'Congo',
   'ch',   'Switzerland',
   'ci',   "Cote D'Ivoire (Ivory Coast)",
   'ck',   'Cook Islands',
   'cl',   'Chile',
   'cm',   'Cameroon',
   'cn',   'China',
   'co',   'Colombia',
   'cr',   'Costa Rica',
   'cs',   'Czechoslovakia (former)',
   'cu',   'Cuba',
   'cv',   'Cape Verde',
   'cx',   'Christmas Island',
   'cy',   'Cyprus',
   'cz',   'Czech Republic',
   'de',   'Germany',
   'dj',   'Djibouti',
   'dk',   'Denmark',
   'dm',   'Dominica',
   'do',   'Dominican Republic',
   'dz',   'Algeria',
   'ec',   'Ecuador',
   'ee',   'Estonia',
   'eg',   'Egypt',
   'eh',   'Western Sahara',
   'er',   'Eritrea',
   'es',   'Spain',
   'et',   'Ethiopia',
   'fi',   'Finland',
   'fj',   'Fiji',
   'fk',   'Falkland Islands (Malvinas)',
   'fm',   'Micronesia',
   'fo',   'Faroe Islands',
   'fr',   'France',
   'fx',   'France, Metropolitan',
   'ga',   'Gabon',
   'gb',   'Great Britain (UK)',
   'gd',   'Grenada',
   'ge',   'Georgia',
   'gf',   'French Guiana',
   'gh',   'Ghana',
   'gi',   'Gibraltar',
   'gl',   'Greenland',
   'gm',   'Gambia',
   'gn',   'Guinea',
   'gp',   'Guadeloupe',
   'gq',   'Equatorial Guinea',
   'gr',   'Greece',
   'gs',   'S. Georgia and S. Sandwich Isls.',
   'gt',   'Guatemala',
   'gu',   'Guam',
   'gw',   'Guinea-Bissau',
   'gy',   'Guyana',
   'hk',   'Hong Kong',
   'hm',   'Heard and McDonald Islands',
   'hn',   'Honduras',
   'hr',   'Croatia (Hrvatska)',
   'ht',   'Haiti',
   'hu',   'Hungary',
   'id',   'Indonesia',
   'ie',   'Ireland',
   'il',   'Israel',
   'in',   'India',
   'io',   'British Indian Ocean Territory',
   'iq',   'Iraq',
   'ir',   'Iran',
   'is',   'Iceland',
   'it',   'Italy',
   'jm',   'Jamaica',
   'jo',   'Jordan',
   'jp',   'Japan',
   'ke',   'Kenya',
   'kg',   'Kyrgyzstan',
   'kh',   'Cambodia',
   'ki',   'Kiribati',
   'km',   'Comoros',
   'kn',   'Saint Kitts and Nevis',
   'kp',   'Korea (North)',
   'kr',   'Korea (South)',
   'kw',   'Kuwait',
   'ky',   'Cayman Islands',
   'kz',   'Kazakhstan',
   'la',   'Laos',
   'lb',   'Lebanon',
   'lc',   'Saint Lucia',
   'li',   'Liechtenstein',
   'lk',   'Sri Lanka',
   'lr',   'Liberia',
   'ls',   'Lesotho',
   'lt',   'Lithuania',
   'lu',   'Luxembourg',
   'lv',   'Latvia',
   'ly',   'Libya',
   'ma',   'Morocco',
   'mc',   'Monaco',
   'md',   'Moldova',
   'mg',   'Madagascar',
   'mh',   'Marshall Islands',
   'mk',   'Macedonia',
   'ml',   'Mali',
   'mm',   'Myanmar',
   'mn',   'Mongolia',
   'mo',   'Macau',
   'mp',   'Northern Mariana Islands',
   'mq',   'Martinique',
   'mr',   'Mauritania',
   'ms',   'Montserrat',
   'mt',   'Malta',
   'mu',   'Mauritius',
   'mv',   'Maldives',
   'mw',   'Malawi',
   'mx',   'Mexico',
   'my',   'Malaysia',
   'mz',   'Mozambique',
   'na',   'Namibia',
   'nc',   'New Caledonia',
   'ne',   'Niger',
   'nf',   'Norfolk Island',
   'ng',   'Nigeria',
   'ni',   'Nicaragua',
   'nl',   'Netherlands',
   'no',   'Norway',
   'np',   'Nepal',
   'nr',   'Nauru',
   'nt',   'Neutral Zone',
   'nu',   'Niue',
   'nz',   'New Zealand (Aotearoa)',
   'om',   'Oman',
   'pa',   'Panama',
   'pe',   'Peru',
   'pf',   'French Polynesia',
   'pg',   'Papua New Guinea',
   'ph',   'Philippines',
   'pk',   'Pakistan',
   'pl',   'Poland',
   'pm',   'St. Pierre and Miquelon',
   'pn',   'Pitcairn',
   'pr',   'Puerto Rico',
   'pt',   'Portugal',
   'pw',   'Palau',
   'py',   'Paraguay',
   'qa',   'Qatar',
   're',   'Reunion',
   'ro',   'Romania',
   'ru',   'Russian Federation',
   'rw',   'Rwanda',
   'sa',   'Saudi Arabia',
   'sb',   'Solomon Islands',
   'sc',   'Seychelles',
   'sd',   'Sudan',
   'se',   'Sweden',
   'sg',   'Singapore',
   'sh',   'St. Helena',
   'si',   'Slovenia',
   'sj',   'Svalbard and Jan Mayen Islands',
   'sk',   'Slovak Republic',
   'sl',   'Sierra Leone',
   'sm',   'San Marino',
   'sn',   'Senegal',
   'so',   'Somalia',
   'sr',   'Suriname',
   'st',   'Sao Tome and Principe',
   'su',   'USSR (former)',
   'sv',   'El Salvador',
   'sy',   'Syria',
   'sz',   'Swaziland',
   'tc',   'Turks and Caicos Islands',
   'td',   'Chad',
   'tf',   'French Southern Territories',
   'tg',   'Togo',
   'th',   'Thailand',
   'tj',   'Tajikistan',
   'tk',   'Tokelau',
   'tm',   'Turkmenistan',
   'tn',   'Tunisia',
   'to',   'Tonga',
   'tp',   'East Timor',
   'tr',   'Turkey',
   'tt',   'Trinidad and Tobago',
   'tv',   'Tuvalu',
   'tw',   'Taiwan',
   'tz',   'Tanzania',
   'ua',   'Ukraine',
   'ug',   'Uganda',
   'uk',   'United Kingdom',
   'um',   'US Minor Outlying Islands',
   'us',   'United States',
   'uy',   'Uruguay',
   'uz',   'Uzbekistan',
   'va',   'Vatican City State (Holy See)',
   'vc',   'Saint Vincent and the Grenadines',
   've',   'Venezuela',
   'vg',   'Virgin Islands (British)',
   'vi',   'Virgin Islands (U.S.)',
   'vn',   'Viet Nam',
   'vu',   'Vanuatu',
   'wf',   'Wallis and Futuna Islands',
   'ws',   'Samoa',
   'ye',   'Yemen',
   'yt',   'Mayotte',
   'yu',   'Yugoslavia',
   'za',   'South Africa',
   'zm',   'Zambia',
   'zr',   'Zaire',
   'zw',   'Zimbabwe',
   'com',  'Commercial',
   'edu',  'Educational',
   'gov',  'Government',
   'int',  'International',
   'mil',  'US Military',
   'net',  'Network',
   'org',  'Non-Profit Organization',
   'arpa', 'Old style Arpanet',
   'nato', 'Nato field',
);

1; # This must be the last line
