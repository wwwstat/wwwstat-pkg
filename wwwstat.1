.\" Copyright (c) 1994, 1996 Regents of the University of California.
.\" All rights reserved.  The file LICENSE (the Artistic License)
.\" specifies the terms and conditions for redistribution.
.\"
.if n .tr \--
.if n .tr \(buo
.if n .tr \(em-
.TH wwwstat 1 "03 November 1996"
.if n .ad l
.SH NAME
wwwstat \- summarize WWW server (httpd) access statistics
.SH SYNOPSIS
.HP 8
.B wwwstat
.RB [ \-F
.IR system_config ]
.RB [ \-f
.IR user_config ]
.RI [ options ...]
.RB [ -- ]
.RI "[ " summary " | " logfile " | "
.BR + " | " \- " ]..."
.SH DESCRIPTION
.nh
.B wwwstat
reads a sequence of
.B httpd
common logfile format (CLF) access_log files and/or prior
.B wwwstat
output summary files and/or the standard input
and outputs a summary of the access statistics in HTML.
.LP
Since
.B wwwstat
does not make any changes to the input files or write any files in the
server directories, it can be run by any user with read access to the input
logfile(s) and summary file(s).  This allows people other than the webmaster
to run specialized analyses of just the things they are interested in 
summarizing.
.LP
.B wwwstat
provides World Wide Web (WWW) access statistics, which does not necessarily
correspond to statistics on individual users. It counts the number of
.B HTTP
requests received by the server
and the amount of bytes transmitted in response to those requests,
according to what is in the logfile(s), and outputs those counts
as tables broken down by category of request.
.LP
.B wwwstat
output summaries can be read by
.B gwstat
to produce fancy graphs of the summarized statistics. The
.B splitlog
program can be used to split a large logfile into separate files by
entry prefix or URL path.
.LP
.B wwwstat
is a
.B perl
script, which means you need to have a
.B perl
interpreter to run the program.  It has been tested with 
.B perl
versions 4.036 and 5.002.
.SS Output Sections
.BR wwwstat 's
output consists of a set of cross-reference links,
the sum totals and averages for the processed data, and
a sequence of amount-by-category tables partitioned into sections.
The section categories are based on the characteristics evident from
the access request, as provided by the common logfile format (see
.BR NOTES ).
These include:
.TP 20
Request Date
e.g., "Feb  2 1996"
.TP
Request Hour
e.g., "00" through "23"
.TP
Client Domain
The Fully-Qualified Domain Name (FQDN) suffix that corresponds to
an organization type or country name.
.TP
Reversed Subdomain
The FQDN, usually minus the first (machine name) component,
and reversed so that it is easier to read when sorted.
.TP
URL/Archive
Grouping based on Request-URI or non-success status code.
.TP
Identity
The user identity based on IdentityCheck token or Authorization field.
.LP
Each section can be enabled/disabled using the configuration files or
command-line options (see
.BR "Section Display Options" ).
.SS Output Table Format
Inside each section, the statistics are presented as a preformatted table.
.LP
.ft C
%Reqs %Byte  Bytes Sent  Requests\0\0
.I category-type
.br
.ft C
----- ----- ------------ -------- |---------------
.br
NN.NN NN.NN NNNNNNNNNNNN NNNNNNNN |
.I category-value
.br
.ft C
100.0 100.0 NNNNNNNNNNNN NNNNNNNN |
.I category-value
.LP
.PD 0
.TP 12
Requests
Requests received for this category-value.
.TP
Bytes Sent
Bytes transmitted for this category-value.
.TP
%Reqs
(<Requests>/<Total Requests>)*100.
.TP
%Byte
(<Bytes Sent>/<Total Bytes>)*100.
.PD
.LP
The table can be sorted by category-value
.RB ( "\-sort key" ),
number of requests received
.RB ( "\-sort req" ),
or number of bytes received
.RB ( "\-sort byte" ).
It can also be limited to the
.BI "\-top" " N"
entries.
.SH OPTIONS
.SS Configuration Options
These options define how
.B wwwstat
should establish defaults and interpret the command-line.
.TP
.BI \-F " filename"
Get system configuration defaults from the given file.  If used, this
.B must
be the first argument on the command-line, since it needs to be interpreted
before the other command options.  The file
.B wwwstat.rc
is included with the distribution as an example of this file; it contains
.B perl
source code which directly sets the control and display options provided by
.BR wwwstat .
If
.I filename
is not a pathname, the include path (see
.BR FILES )
is searched for
.IR filename .
An empty string as
.I filename
will disable this feature.
.RB [ \-F
"wwwstat.rc"]
.TP
.BI \-f " filename"
Get user configuration defaults from the given file. If used, this
.B must
be the first argument on the command-line after
.B \-F
(if any). The file is the same format as for the
.B \-F
option (see
.BR wwwstat.rc ).
If
.I filename
is not a pathname, the include path (see
.BR FILES )
is searched for
.IR filename .
An empty string as
.I filename
will disable this feature.
.RB [ \-f
".wwwstatrc"]
.TP
.B --
Last option (the remaining arguments are treated as input files).
.SS Diagnostic Options
These options provide information about
.B wwwstat
usage or about some unusual aspects of the logfile(s) being processed.
.TP
.B \-h
Help \(em display usage information to STDERR and then exit.
.TP
.B \-v
Verbose display to STDERR of each log entry processed.
.TP
.B \-x
Display to STDERR all requests resulting in HTTP error responses.
.TP
.B \-e
Display to STDERR all invalid log entries. Invalid log entries can occur
if the server is miswriting or overwriting its own log, if the request is
made by a broken client or proxy, or if a malicious attacker is trying to
gain privileged access to your system.  For the latter reason, the webmaster
should run
.B wwwstat
with this option on a regular basis.
.SS Display Options
These options modify the output format.
.TP
.BI \-H " string"
Use the given string as the HTML title and heading for output.
.TP
.BI \-X " string"
Use the given string as the cross-reference URL to the last summary output.
Any occurrence of the characters "%M" or "%Y" are replaced by the month
and year, respectively, of the month prior to the first log entry date.
The empty string will exclude any cross-reference.
.TP
.B \-R
Display the daily stats table sorted in reverse. This option is primarily
for use with the
.B gwstat
program for producing graphs of the output.
.LP
.PD 0
.B \-l
.TP
.B \-L
.PD
Do
.RB ( \-l )
or don't
.RB ( \-L )
display the full DNS hostname of clients in your local domain (which is
determined by the configured value of $AppendToLocalhost) in the section
on subdomain statistics.  The default
.RB [ \-L ]
is to strip the machine name from local addresses.
.LP
.PD 0
.B \-o
.TP
.B \-O
.PD
Do
.RB ( \-o )
or don't
.RB ( \-O )
display the full DNS hostname of clients outside your local domain
in the section on subdomain statistics.  The default
.RB [ \-O ]
is to strip the machine name from outside addresses.
.LP
.PD 0
.B \-u
.TP
.B \-U
.PD
Do
.RB ( \-u )
or don't
.RB ( \-U )
display the IP address of clients with unresolved domain names in the section
on subdomain statistics. The
.B \-dns
option can be used to resolve some names, but not all IP hosts have
a DNS name (SLIP/PPP connections) and sometimes a host's DNS service
is inaccessible. The default
.RB [ \-U ]
is to group all such addresses under the category "Unresolved".
.LP
.PD 0
.B \-dns
.TP
.B \-nodns
.PD
Do
.RB ( \-dns )
or don't
.RB ( \-nodns )
use the system's hostname lookup facilities to find the DNS hostname
associated with any unresolved IP addresses. Looking up a DNS name may be
.B very
slow, particularly when the results are negative (no DNS name),
which is why a caching capability is included as well.
.RB [ \-nodns ]
.TP
.BI \-cache " filename"
Use the given DBM database as the read/write persistent DNS cache
(the .dir and .pag extensions are appended automatically). Cached entries
(including negative results) are removed after the time configured for
$DNSexpires [two months].  No caching is performed if
.I filename
is the empty string, which may be needed if your system does not support
DBM or NDBM functionality. Running
.B \-dns
without a persistent cache is not recommended.
.RB [ "\-cache"
"dnscache"]
.TP
.BI \-trunc " N"
Truncate the URLs listed in the archive section after the
.I Nth
hierarchy level. This option is commonly used to reduce the output size
and memory requirements of
.B wwwstat
by grouping the requests by directory tree instead of listing every URL.
The default
.RB [ "\-trunc 0" ]
is to display every requested URL.
.LP
.PD 0
.B \-files
.TP
.B \-nofiles
.PD
Do
.RB ( \-files )
or don't
.RB ( \-nofiles )
include the last component of a URL (usually the filename) in the
archive section. This option is commonly used to reduce the output size
and memory requirements of
.B wwwstat
by grouping the requests by directory instead of listing every URL.
The default
.RB [ \-files ]
is to display the entire requested URL.
.LP
.PD 0
.B \-link
.TP
.B \-nolink
.PD
Do
.RB ( \-link )
or don't
.RB ( \-nolink )
add a hypertext link around each archive URL.  This option is useful for
local maintenance, but it is not recommended for publication of the HTML
results (it often results in links to temporary or nonexistant resources,
and leads people/robots to resources that might not be publically available).
.RB [ \-nolink ]
.LP
.PD 0
.B \-cgi
.TP
.B \-nocgi
.PD
Do
.RB ( \-cgi )
or don't
.RB ( \-nocgi )
prefix the summary output with CGI header fields appropriate for use
with the HTTP common gateway interface.  Using
.B wwwstat
as a CGI script is not recommended \(em it is usually better to simply
run the wwwstat program periodically and serve the static output file.
.RB [ \-nocgi ]
.SS Section Display Options
These options change the display of entire sections (as opposed to the
entries within those sections).  They allow the user to enable or disable
an entire section, set the sorting method for that section, and limit the
number of displayed entries for that section.  These options are
context-sensitive and processed in the order given.
.LP
.PD 0
.B \-all
.TP
.B \-noall
.PD
Include
.RB ( \-all )
or exclude
.RB ( \-noall )
all of the display sections. The
.B \-noall
option is commonly used just prior to one or more of the other section
options, such that only the listed sections are displayed.
.LP
.PD 0
.B \-daily
.TP
.B \-nodaily
.PD
Include
.RB ( \-daily )
or exclude
.RB ( \-nodaily )
the section of statistics by
request date
and set the scope for later
.BR \-sort " and " \-top
options to this section.
.LP
.PD 0
.B \-hourly
.TP
.B \-nohourly
.PD
Include
.RB ( \-hourly )
or exclude
.RB ( \-nohourly )
the section of statistics by
request hour
and set the scope for later
.BR \-sort " and " \-top
options to this section.
.LP
.PD 0
.B \-domain
.TP
.B \-nodomain
.PD
Include
.RB ( \-domain )
or exclude
.RB ( \-nodomain )
the section of statistics by
the client's Internet domain
and set the scope for later
.BR \-sort " and " \-top
options to this section.
.LP
.PD 0
.B \-subdomain
.TP
.B \-nosubdomain
.PD
Include
.RB ( \-subdomain )
or exclude
.RB ( \-nosubdomain )
the section of statistics by
the client's Internet subdomain (reversed for display)
and set the scope for later
.BR \-sort " and " \-top
options to this section.
.LP
.PD 0
.B \-archive
.TP
.B \-noarchive
.PD
Include
.RB ( \-archive )
or exclude
.RB ( \-noarchive )
the section of statistics by
requested URL/archive
and set the scope for later
.BR \-sort " and " \-top
options to this section.
.LP
.PD 0
.B \-r
.LP
.B \-ident
.TP
.B \-noident
.PD
Include
.RB ( \-r " or " \-ident )
or exclude
.RB ( \-noident )
the section of statistics by
the identity of the user (if IdentityCheck is ON) or the authentication
userid (if supplied)
and set the scope for later
.BR \-sort " and " \-top
options to this section.
.B DO NOT PUBLISH
this information, as that would reveal security-related identities
and be a violation of privacy.  This option is provided for administrative
purposes only.
.TP
.BR \-sort " (" "key|byte|req" )
Sort this section by its primary key, the number of bytes transmitted,
or the number of requests received.
.RB [ "\-sort key" ]
.TP
.BI \-top " N"
Display only the top N entries for this section. This option assumes that
the
.B \-sort
option has been set to either bytes or requests.
.TP
.B \-both
Display both the top N entries for this section [10, sorted by requests],
and then the full section (all entries) sorted by key.
.SS Search Options
These options are used to limit the analysis to requests matching a
pattern.  The pattern is supplied in the form of a
.BR "perl regular expression" ,
except that the characters "+" and "." are escaped automatically
unless the
.B \-noescape
option is given.
Enclose the pattern in single-quotes to prevent the command shell
from interpreting some special characters.
.LP
Multiple occurrences of the same option results in an OR-ing of the
regular expressions.  Search options are only applied to logfile entries;
any summary files input must have been created with the same search options.
.LP
.PD 0
.BI \-a " regexp"
.TP
.BI \-A " regexp"
.PD
Include
.RB ( \-a )
or exclude
.RB ( \-A )
all requests containing a hostname/IP address
matching the given perl regular expression.
.LP
.PD 0
.BI \-c " regexp"
.TP
.BI \-C " regexp"
.PD
Include
.RB ( \-c )
or exclude
.RB ( \-C )
all requests resulting in an
.B HTTP
status code
matching the given perl regular expression.
.LP
.PD 0
.BI \-d " regexp"
.TP
.BI \-D " regexp"
.PD
Include
.RB ( \-d )
or exclude
.RB ( \-D )
all requests occurring on a date (e.g., "Feb  2 1994")
matching the given perl regular expression.
.LP
.PD 0
.BI \-t " regexp"
.TP
.BI \-T " regexp"
.PD
Include
.RB ( \-t )
or exclude
.RB ( \-T )
all requests occurring during the hour (e.g., "23" is 11pm \- 12pm)
matching the given perl regular expression.
.LP
.PD 0
.BI \-m " regexp"
.TP
.BI \-M " regexp"
.PD
Include
.RB ( \-m )
or exclude
.RB ( \-M )
all requests using an HTTP method (e.g., "HEAD")
matching the given perl regular expression.
.LP
.PD 0
.BI \-n " regexp"
.TP
.BI \-N " regexp"
.PD
Include
.RB ( \-n )
or exclude
.RB ( \-N )
all requests on a URL (archive name)
matching the given perl regular expression.
.TP
.B \-noescape
Do not escape the special characters ("+" and ".") in the remaining
search options.
.SH INPUT
After parsing the options, the remaining arguments on the command-line
are treated as input arguments and are read in the order given.
If no input arguments are given, the configured default logfile is read
.RB [ + ].
.TP
.B \-
Read from standard input (STDIN).
.TP
.B +
Read the default logfile. [as configured]
.TP
.IR filename ...
Read the given file and determine from the first line whether it is a
previous output summary or a CLF logfile.  If the
.IR filename 's
extension indicates that is is compressed (gz|z|Z), then pipe it through
the configured decompression program
.RB [ "gunzip \-c" ]
first. Summary files must have been created with the same (or similar)
configuration and command-line options as the currently running program;
if not, weird things will happen.
.SH USAGE
.B wwwstat
is used for many purposes:
.TP
  \(bu
as a diagnostic utility for measuring server activity, finding incorrect
URL references, and detecting attempted misuse of the server;
.TP
  \(bu
as a public relations tool for measuring technology or information transfer
(i.e., Is the message getting out? To the right people?);
.TP
  \(bu
as an archival tool for tracking web usage over time without storing
the entire logfile; and,
.TP
  \(bu
most often, as an easy mechanism for justifying all the hard work that went
into creating the web content that people out there are requesting.
.LP
In most cases,
.B wwwstat
is run on a periodic basis (nightly, weekly, and/or monthly) by a wrapper
program as a
.B crontab
entry shortly after midnight, typically in conjunction
with rotating the current logfile.  The output is usually directed
to a temporary file which can later be moved to a published location.
The temporary file is necessary to avoid erasing your published file
during wwwstat's processing (which would look very odd if someone tried
to GET it from your web).
.LP
.B wwwstat
can be run as a CGI script
.RB ( \-cgi ),
but that is not recommended unless the input logfile is very small.
.LP
All of the command-line options, and a few options that are not available
from the command-line, can be changed within the user and system configuration
files (see
.BR wwwstat.rc ).
These files are actually
.B perl
library modules which are executed as part of the program's initialization.
The example provided with the distribution includes complete documentation
on what variables can be set and their range of values.
.SS Perl Regular Expressions
The Search Options and many of the configuration file settings
allow for full use of perl regular expressions
(with the exception that the \-a, \-A, \-n and \-N options treat '+' and '.'
characters as normal alphabetic characters unless they are preceded by the
.B \-noescape
option).  Most people only need to know the following special characters:
.LP
.PD 0
.TP 8
.B ^
at start of pattern, means "starts with pattern".
.TP
.B $
at end of pattern, means "ends with pattern".
.TP
.B (...)
groups pattern elements as a single element.
.TP
.B ?
matches preceding element zero or one times.
.TP
.B *
matches preceding element zero or more times.
.TP
.B +
matches preceding element one or more times.
.TP
.B .
matches any single character.
.TP
.B [...]
denotes a class of characters to match. [^...] negates the class.
Inside a class, '-' indicates a range of characters.
.TP
.B (A|B|C)
matches if A or B or C matches.
.PD
.LP
Depending on your command shell, some special characters may need to be
escaped on the command line or enclosed in single-quotes to avoid shell
interpretation.
.SH EXAMPLES
.TP
Summarize requests from commercial domains.
.B wwwstat \-a '.com$'
.TP
Summarize requests from the host kiwi.ics.uci.edu
.B wwwstat \-a '^kiwi.ics.uci.edu$'
.TP
Summarize requests not from kiwi.ics.uci.edu
.B wwwstat \-A '^kiwi.ics.uci.edu$'
.TP
Summarize requests resulting in temporary redirects
.B wwwstat \-c '302'
.TP
Summarize requests resulting in server errors
.B wwwstat \-c '^5'
.TP
Summarize unsuccessful requests
.B wwwstat \-C '^2' \-C '304'
.TP
Summarize requests in first week of the month
.B wwwstat \-d ' [1-7] '
.TP
Summarize requests in second week of the month
.B wwwstat \-d ' ([89]|1[0-4]) '
.TP
Summarize requests in third week of the month
.B wwwstat \-d ' (1[5-9]|2[01]) '
.TP
Summarize requests in fourth week of the month
.B wwwstat \-d ' 2[2-8] '
.TP
Summarize requests in leftover days of the month
.B wwwstat \-d ' (29|30|31) '
.TP
Summarize requests in February
.B wwwstat \-d 'Feb'
.TP
Summarize requests in year 1994
.B wwwstat \-d '1994'
.TP
Summarize requests not in April
.B wwwstat \-D 'Apr'
.TP
Summarize requests between midnight and 1am
.B wwwstat \-t '00'
.TP
Summarize requests not received between noon and 1pm
.B wwwstat \-T '12'
.TP
Summarize requests with a gif extension
.B wwwstat \-n '.gif$'
.TP
Summarize requests under user's URL
.B wwwstat \-n '^/~user/'
.TP
Summarize requests not under "hidden" paths
.B wwwstat \-N '/hidden/'
.SH ENVIRONMENT
.TP 12
.B HOME
Location of user's home directory, placed on INC path.
.TP
.B LOGDIR
Used instead of HOME if latter is undefined.
.TP
.B PERLLIB
A colon-separated list of directories in which to look for
include and configuration files.
.SH FILES
Unless a pathname is supplied, the configuration files are obtained from
the current directory, the user's home directory
.RB ( HOME " or " LOGDIR ),
the standard library path
.RB ( PERLLIB ),
and the directory indicated by the command pathname (in that order).
.TP 15
.B .wwwstatrc
User configuration file.
.TP
.B wwwstat.rc
System configuration file.
.TP
.B domains.pl
Mapping of Internet domain to country or organization.
.LP
.PD 0
.B dnscache.dir
.TP 15
.B dnscache.pag
.PD
DBM files for persistent DNS cache.
.SH SEE ALSO
.BR crontab (1),
.BR gwstat (1),
.BR httpd (1m),
.BR perl (1),
.BR splitlog (1)
.LP
More info and the latest version of wwwstat can be obtained from
.LP
     http://www.ics.uci.edu/pub/websoft/wwwstat/
      ftp://www.ics.uci.edu/pub/websoft/wwwstat/
.LP
If you have any suggestions, bug reports, fixes, or enhancements,
please join the <wwwstat-users@ics.uci.edu> mailing list by sending
e-mail with "subscribe" in the subject of the message to the request
address <wwwstat-users-request@ics.uci.edu>.  The list is archived at
the above address.
.SS More About HTTP
.TP
HTTP/1.1 Proposed Standard
R. Fielding, J. Gettys, J. C. Mogul, H. Frystyk, and T. Berners-Lee.
"Hypertext Transfer Protocol -- HTTP/1.1", U.C. Irvine, DEC, MIT/LCS,
August 1996.
.br
http://www.ics.uci.edu/pub/ietf/http/
.LP
.SS More About Perl
.TP
The Perl Language Home Page
http://www.perl.com/perl/index.html
.TP
Johan Vromans' Perl Reference Guide
http://www.xs4all.nl/~jvromans/perlref.html
.SH DIAGNOSTICS
See also the
.B Diagnostic Options
above.
.TP
"[none] to [none]" dates
.B wwwstat
did not find any matching data to summarize.  If you get such an empty
summary, it means that either:
1) there was no valid data (the input files are all invalid or empty), or
2) none of the data matched the search options given.  Try using the
.B \-e
option to show invalid data.
.TP
100% unresolved
If the subdomain section indicates that all of the client requests come
from unresolved hostnames (IP addresses), this probably means that your
server is running without DNS resolution (common for very busy sites).
You can use the
.B \-dns
option to have
.B wwwstat
perform the hostname lookups.  If 100% of the hosts are still unresolved
with the 
.B \-dns
option in effect, then it may be that all of the clients accessing your
server are doing so from temporary SLIP/PPP addresses without DNS names, or
it may be a problem with wwwstat's DNS cache (delete the cache files),
with your system's DNS software (contact your system administrator),
or with your network connection.
.SH NOTES
.SS Hits vs Requests vs Visitors
.B wwwstat
counts HTTP requests received by the server.  When a request is successful,
it is often referred to as a "hit". Retrieving a single image is one GET
request. Retrieving an HTML page is also one GET request, but that does not
include the separate requests made for in-line images or related objects.
Checking to see if a cached image is still valid (a HEAD or conditional GET)
is also one request.
.LP
In all sections except the archive section,
.B wwwstat
shows the statistics for all requests (successful or not).  In the archive
section, it normally shows all non-successful requests under a special category
for the status code and only successful requests (hits) under the URL or
archive tree associated with the request.  However, this grouping of
non-successful requests is disabled when
.B wwwstat
is used with the search options
.BR \-n ", " \-c ", and " \-C ","
since those options are normally used for finding error conditions.
.LP
.B wwwstat
does not count "visitors" -- individual people or programs making the
requests. HTTP does not, by default, provide any information that can be
accurately correlated to an individual person, though it is possible
(in an unreliable manner) to use HTTP extensions and request profiles
as a means of tracking individual client programs.  Such tracking
requires extensive resources (memory and diskspace) and is often considered
a violation of privacy.
.LP
With the exception of the ident section,
.B wwwstat
does not reveal information about the individual people making requests.
Unless the output is limited to a specific URL or a specific hostname,
.BR wwwstat 's
output does not connect the requester to the URL being requested.
.SS Common Logfile Format
The httpd common logfile format (CLF) was defined in early 1994 as the
result of discussions among server and access_log analyzer developers
(Roy Fielding, John Franks, Kevin Hughes, Ari Luotonen, Rob McCool,
and Tony Sanders) on how to make it easier for analysis tools to be
used across multiple servers.  The format is:
.LP
remote_host ident authuser [date-time zone] "Request-Line" Status-Code bytes
.LP
.PD 0
.TP 15
where
means
.TP
------------
--------------------------------------
.TP
remote_host
Client DNS hostname or IP address
.TP
ident
Identity check token or "\-"
.TP
authuser
Authorization user-id or "\-"
.TP
date-time
dd/Mmm/yyyy:hh:mm:ss
.TP
zone
+dddd or \-dddd
.TP
Request-Line
The first line of the HTTP request, which normally includes the
method, URL, and HTTP-version.
.TP
Status-Code
Response status from server or "\-"
.TP
bytes
Size of Entity-Body transmitted or "\-"
.TP
------------
--------------------------------------
.PD
.LP
with each field separated by a single space (it turns out that problems
occur if the ident token contains a space, which was not anticipated
by the original designers).
.SH LIMITATIONS
.B wwwstat
cannot be more accurate than its input.
.LP
The common logfile format does not include the amount of bytes transferred
in HTTP header fields and in error responses.
.B wwwstat
attempts to estimate those bytes based on the response code.  Although
the built-in estimates will suffice for most applications, your results
will be more accurate if the estimates are customized for the particular
server software that generated the logfile.
.LP
Modern httpd servers have extended the CLF to include additional fields
(Referer and User-Agent) or to make the entire format configurable.
Although
.B wwwstat
is able to read logfiles which append information to the CLF, it
will not make use of that additional information.  However,
.B wwwstat
is written in
.BR perl ,
so if you want to parse a different format all you have to do is change
the parsing code.
.LP
.B wwwstat
does not do anything with Referer [sic] or User-Agent information that may be
present in extended logfiles.  In order to do anything interesting with
Referer, the program would have to build a Request-URI x Referer x Count
table, which would require huge gobs of memory and is better done using
a separate program with a persistent database.  Naturally, this is easy
to do once you learn
.BR perl .
.SH AUTHOR
Roy Fielding (fielding@ics.uci.edu), University of California, Irvine.
Please do not send questions or requests to the author, since the number
of requests has long since overwhelmed his ability to reply, and all
future support will be through the mailing list (see above).
.LP
.B wwwstat
was originally based on a multi-server statistics program called
.B fwgstat-0.035
by Jonathan Magid (jem@sunsite.unc.edu) which, in turn, was heavily based on
.B xferstats
(packaged with the version 17 of the Wuarchive FTP daemon)
by Chris Myers (chris@wugate.wustl.edu).
.LP
This work has been sponsored in part by the Defense Advanced Research Projects
Agency under Grant Numbers MDA972-91-J-1010 and F30602-94-C-0218.
This software does not necessarily reflect the position or policy of the
U.S. Government and no official endorsement should be inferred.
